#language: pt

Funcionalidade: CalculoPrecoPrazoEntrega
  Como um usuário, eu gostaria de realizar o cálculo de frete por tipo de serviço
	Para saber o valor do frete entre dois CEPs distintos
	Porque dessa forma fica fácil decidir qual tipo de serviço utilizar

Esquema do Cenário: Cálculo de Preço e Prazo de Entrega por Tipo de Serviço
	Dado que estou na página de Cálculo de Preço e Prazo de Entrega dos Correios
	Quando preencho o campo de CEP Origem "<cepOrigem>", CEP Destino "<cepDestino>" e Tipo de serviço "<tipoServico>"
	E envio o formulário
	Então o sistema deve retornar o cálculo de preço e prazo de entrega pelo "<tipoServico>"

	Exemplos: 
	| cepOrigem | cepDestino | tipoServico        |
	| 20090080  | 20081240   | Carta Registrada   |
	| 20090080  | 20051070   | Carta Simples      |
	| 20090080  | 20051070   | Carta Via Internet |