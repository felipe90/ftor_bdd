#language: pt

@funcionais
Funcionalidade: Correios - Calculo de Preço e Prazo de Entrega
  Como um usuário, eu gostaria de realizar o cálculo de frete por tipo de serviço
	Para saber o valor do frete entre dois CEPs distintos
	Porque dessa forma fica fácil decidir qual tipo de serviço utilizar

Cenário: Cálculo pelo Tipo de Serviço Carta Registrada
	Dado que estou na página de Cálculo de Preço e Prazo de Entrega dos Correios
	Quando preencho o campo de CEP Origem "20090080", CEP Destino "20081240" e Tipo de serviço "Carta Registrada"
	E envio o formulário
	Então o sistema deve retornar o cálculo de preço e prazo de entrega pelo "Carta Registrada"
	
Cenário: Cálculo pelo Tipo de Serviço Carta Simples
	Dado que estou na página de Cálculo de Preço e Prazo de Entrega dos Correios
	Quando preencho o campo de CEP Origem "20090080", CEP Destino "20051070" e Tipo de serviço "Carta Simples"
	E envio o formulário
	Então o sistema deve retornar o cálculo de preço e prazo de entrega pelo "Carta Simples"
	
Cenário: Cálculo pelo Tipo de Serviço Carta Via Internet
	Dado que estou na página de Cálculo de Preço e Prazo de Entrega dos Correios
	Quando preencho o campo de CEP Origem "20090080", CEP Destino "20051070" e Tipo de serviço "Carta Via Internet"
	E envio o formulário
	Então o sistema deve retornar o cálculo de preço e prazo de entrega pelo "Carta Via Internet"