package pages;

import org.openqa.selenium.NoSuchElementException;

import elements_pages.ElementsPageConsultaPrecoPrazoEntrega;
import hooks.Constants;
import utils.Utils;

public class PageConsultaPrecoPrazoEntrega {
	
	private ElementsPageConsultaPrecoPrazoEntrega elementos = new ElementsPageConsultaPrecoPrazoEntrega();
    Utils util = new Utils();
    
    public void acessarURLPrecoPrazoEntrega() throws Exception
    {
    	util.acessarURL(Constants.URL_CONSULTA_PRECO_PRAZO_ENTREGA);
    }
    
    public void validarAcessoAPaginaDeConsultaDosCorreios() throws NoSuchElementException
    {
    	util.elementoEstaVisivel(elementos.GetCepOrigem()); 
    }
    
    public void informarCepOrigem(String cep_origem) throws Exception
    {
    	util.escreverTexto(elementos.GetCepOrigem(), cep_origem); 
    }

    public void informarCepDestino(String cep_destino) throws Exception
    {
        util.escreverTexto(elementos.GetCepDestino(),cep_destino);
    }

    public void selecionarTipoServico(String tipo_servico) throws Exception
    {
        util.selecionarElementoPorTexto(elementos.GetTipoServico(), tipo_servico);
    }

    public void clicarBotaoEnviar() throws Exception
    {
        util.clicarElemento(elementos.GetEnviar());
    }
}
