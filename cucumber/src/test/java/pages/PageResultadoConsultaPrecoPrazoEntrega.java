package pages;

import hooks.Constants;
import utils.Utils;

public class PageResultadoConsultaPrecoPrazoEntrega {
	
	Utils util = new Utils();
	
	public void validarConsultaPrecoPrazoEntrega(String tipo_servico) throws Exception
    {
    	String urlResultadoConsulta = Constants.URL_RESULTADO_CONSULTA_PRECO_PRAZO_ENTREGA;
    	util.alterarJanelaPorURL(urlResultadoConsulta);
        util.buscarPorTextoNaPagina(tipo_servico);
        util.validarURLAtualContemString("cfm");
    }
}
