package steps;


import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.PageConsultaPrecoPrazoEntrega;
import pages.PageResultadoConsultaPrecoPrazoEntrega;
import runners.RunnerTest;
import utils.Utils;

public class ConsultarPrecoPrazoEntregaSteps {
	
	PageConsultaPrecoPrazoEntrega page_consulta_preco_prazo = new PageConsultaPrecoPrazoEntrega();
	PageResultadoConsultaPrecoPrazoEntrega page_resultado_consulta_preco_prazo = new PageResultadoConsultaPrecoPrazoEntrega();
	
	@Dado("^que estou na página de Cálculo de Preço e Prazo de Entrega dos Correios$")
	public void queEstouNaPaginadeCalculoPrecoPrazoEntrega() throws Throwable {
		page_consulta_preco_prazo.acessarURLPrecoPrazoEntrega();
		page_consulta_preco_prazo.validarAcessoAPaginaDeConsultaDosCorreios();
		
	}
	
	@Quando("^preencho o campo de CEP Origem \"([^\"]*)\", CEP Destino \"([^\"]*)\" e Tipo de serviço \"([^\"]*)\"$")
	public void preenchoOsCamposParaConsultaPrecoPrazoEntrega(String cep_origem, String cep_destino, String tipo_servico) throws Throwable {
		page_consulta_preco_prazo.informarCepOrigem(cep_origem);
		page_consulta_preco_prazo.informarCepDestino(cep_destino);
		page_consulta_preco_prazo.selecionarTipoServico(tipo_servico);
	}
	
	@E("^envio o formulário$")
	public void clicarBotaoEnviar() throws Throwable {
		page_consulta_preco_prazo.clicarBotaoEnviar();
	}
	
	@Entao("^o sistema deve retornar o cálculo de preço e prazo de entrega pelo \"([^\"]*)\"$")
	public void validarResultadoConsultaPrecoPrazoEntrega(String tipo_servico) throws Throwable {
		page_resultado_consulta_preco_prazo.validarConsultaPrecoPrazoEntrega(tipo_servico);
	}
}
