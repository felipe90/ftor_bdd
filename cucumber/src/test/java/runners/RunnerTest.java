package runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import hooks.Hooks;


@RunWith(Cucumber.class) 
@CucumberOptions(features = "src/test/resources/features/",
				 plugin = {"pretty", "html:target/report-html", "json:target/report.json"},
				 glue = "steps",
				 tags = {"@funcionais"},
				 monochrome = false,
                 snippets = SnippetType.CAMELCASE,
                 dryRun = false,
                 strict = true
				 ) 

public class RunnerTest extends Hooks {	
	
	
}