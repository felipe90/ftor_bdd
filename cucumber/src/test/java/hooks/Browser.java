package hooks;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import hooks.Hooks;
import io.github.bonigarcia.wdm.WebDriverManager;
import runners.RunnerTest;
import utils.Utils;

public class Browser {
	
	public static WebDriver driver;
	
	public static FirefoxOptions options;
	public static String path_Firefox_Portable_44 = (Utils.userDir + "\\Tools\\FirefoxPortable44\\FirefoxPortable.exe");
	public static String path_Gecko_Driver_018 = Utils.userDir + "\\drivers\\geckodriver_018.exe";
	public static String path_Chrome_Portable_67 = (Utils.userDir + "\\tools\\GoogleChromePortable67\\GoogleChromePortable.exe");
	public static String path_Chrome_Driver_2_40 = Utils.userDir + "\\drivers\\chromedriver_2.40.exe";
	public static String path_IE_Driver_Server_3_14 = Utils.userDir + "\\drivers\\IEDriverServer_3.14.exe";
	public static String path_Microsoft_WebDriver_v6 = Utils.userDir + "\\drivers\\MicrosoftWebDriver_v6.exe";
//	public static String path_Opera_Driver_2_40 = Utils.userDir + "\\drivers\\operadriver_2.40.exe";
//	public static String path_Opera = "C:\\Users\\" + Utils.userName + "\\AppData\\Local\\Programs\\Opera\\launcher.exe";
	public static String path_PhantomJS_2_11 = (Utils.userDir + "\\tools\\PhantomJS_211\\bin\\phantomjs.exe");

	
	public static void executarComFirefoxPortable44() {
		/*****************************************
		 * Firefox Portable 44 + geckodriver 0.18
		 *****************************************/
		System.setProperty("webdriver.gecko.driver", path_Gecko_Driver_018);
		System.setProperty("webdriver.firefox.bin", path_Firefox_Portable_44);

		options = new FirefoxOptions();
//		options.setLegacy(true);

		FirefoxProfile firefoxProfile = new FirefoxProfile();
		firefoxProfile.setPreference("browser.privatebrowsing.autostart", true);

		driver = new FirefoxDriver(options);
		configurarNovaInstanciaBrowser();
	}

	public static void executarComChrome() {
		/********************************
		 * Chrome (vers�o instalada na m�quina) + chromedriver 2.40
		 ********************************/
		System.setProperty("webdriver.chrome.driver", path_Chrome_Driver_2_40);

		driver = new ChromeDriver();
		configurarNovaInstanciaBrowser();
	}

	public static void executarComIE11() {
		/**********************************************
		 * Internet Explorer 11 + IEDriver Server 3.14.0
		 **********************************************/
		System.setProperty("webdriver.ie.driver", path_IE_Driver_Server_3_14);
		
		InternetExplorerOptions ie_options = new InternetExplorerOptions();

		ie_options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);

		driver = new InternetExplorerDriver(ie_options);
		configurarNovaInstanciaBrowser();
	}

	public static void executarComEdge17() {
		/****************************************
		 * Edge 17 + Microsoft WebDriver vers�o 6
		 ****************************************/
		System.setProperty("webdriver.edge.driver", path_Microsoft_WebDriver_v6);

		driver = new EdgeDriver();
		configurarNovaInstanciaBrowser();
	}

	public static void executarComPhantomJS_2_11() {
		/******************
		 * PhantomJS 2.11
		 ******************/
		DesiredCapabilities caps = new DesiredCapabilities();
		//caps.setJavascriptEnabled(true);
		caps.setCapability("takesScreenshot", true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, path_PhantomJS_2_11);
		driver = new PhantomJSDriver(caps);
	}

	public static void executarComChrome_Headless() {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		
		driver = new ChromeDriver(options);
		
		configurarNovaInstanciaBrowser();
	}

	public static void executarComChromePortable67() {
		/****************************************
		 * Chrome Portable 67 + chromedriver 2.40
		 ****************************************/
		String proxyHost = null;
		System.setProperty("webdriver.chrome.driver", path_Chrome_Driver_2_40);

		DesiredCapabilities extraCapabilities = new DesiredCapabilities();
		extraCapabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		extraCapabilities.setPlatform(Platform.WINDOWS);

		Proxy proxy = new Proxy();
		proxy.setHttpProxy(proxyHost).setFtpProxy(proxyHost).setSslProxy(proxyHost);
		extraCapabilities.setCapability(CapabilityType.PROXY, proxy);

		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("binary", path_Chrome_Portable_67);
		extraCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

		driver = new ChromeDriver();
	    configurarNovaInstanciaBrowser();
	}

//	public static void executarComOpera56() {
//		/*****************************
//		 * Opera 56 + operadriver 2.40
//		 *****************************/
//		OperaOptions options = new OperaOptions();
//		options.setBinary(path_Opera);
//		System.setProperty("webdriver.opera.driver", path_Opera_Driver_2_40);
//		driver = new OperaDriver(options);
//		configurarNovaInstanciaBrowser();
//	}

	public static void fecharInstancia() {
		driver.quit();
	}

	public static void configurarNovaInstanciaBrowser() {
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}
	
	public static void executarComRemoteWebDriverChrome() throws MalformedURLException
	{
		ChromeOptions chrome_options = new ChromeOptions();
		
		chrome_options.setPlatformName("Windows");
		driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chrome_options);
	}
	
	public static void executarComRemoteWebDriverFirefox() throws MalformedURLException
	{
		FirefoxOptions firefox_options = new FirefoxOptions();
		
		firefox_options.setPlatformName("Windows");
		driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), firefox_options);
	}
	
	public static void executarComRemoteWebDriverInternetExplorer() throws MalformedURLException
	{
		InternetExplorerOptions ie_options = new InternetExplorerOptions();
		
		ie_options.setPlatformName("Windows");
		
		driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), ie_options);
	}
	
	public static void executarComRemoteWebDriverPhantomJS() throws MalformedURLException
	{
//		DesiredCapabilities caps = DesiredCapabilities.phantomjs();
//		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, path_PhantomJS_2_11);
//		driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), caps);
	}

}
