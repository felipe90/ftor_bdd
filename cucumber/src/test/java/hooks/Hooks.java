package hooks;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import utils.Utils;

public class Hooks  {
	
	@BeforeClass
	public static void setUp() throws Exception {
		Browser.executarComChrome_Headless();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		Browser.fecharInstancia();
	}
}