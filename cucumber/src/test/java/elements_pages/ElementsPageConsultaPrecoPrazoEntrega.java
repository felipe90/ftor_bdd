package elements_pages;

import org.openqa.selenium.By;

public class ElementsPageConsultaPrecoPrazoEntrega {
	
	public String url_consulta_preco_prazo_entrega = "http://www2.correios.com.br/sistemas/precosPrazos/";
	public String url_resultado_consulta_preco_prazo_entrega = "http://www2.correios.com.br/sistemas/precosPrazos/prazos.cfm";
	private By TxtCepOrigem = By.name("cepOrigem");
    private By TxtCepDestino = By.name("cepDestino");
    private By CmbTipoServico = By.name("servico");
    private By BtnEnviar = By.name("Calcular");

    public By GetCepOrigem()
    {
        return TxtCepOrigem;
    }

    public By GetCepDestino()
    {
        return TxtCepDestino;
    }

    public By GetTipoServico()
    {
        return CmbTipoServico;
    }

    public By GetEnviar()
    {
        return BtnEnviar;
    } 
}
