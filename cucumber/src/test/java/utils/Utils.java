package utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import hooks.Browser;
import hooks.Hooks;
import runners.RunnerTest;
import org.junit.Assert;


public class Utils extends Browser{
	
	
	public static String userDir = System.getProperty("user.dir");
	public static String userName = System.getProperty("user.name");
	public static String osName = System.getProperty("os.name");
	public static String osVersion = System.getProperty("os.version");
	public static String osArchitecture = System.getProperty("os.arch");
	public static String nomeSistema = "Correios - Preços e Prazos"; 
	public static String nomeSuite = "Cálculo do Preço e Prazo de Entrega";
	
	
	// Aceita o alerta (clica no bot�o OK).
		public void aceitarAlerta() throws Exception {
			Alert alert = driver.switchTo().alert();
			alert.accept();	
		}

		// Acessa � URL passada por par�metro.
		public void acessarURL(String url) throws Exception {
			driver.get(url);
		}

		/*
		 * Aguarda o alert ser exibido (wait din�mico). Quando o mesmo estiver vis�vel,
		 * o teste ir� prosseguir. O tempo limite � de 30 segundos.
		 */
		public void aguardarPorAlert() throws Exception {
			Duration tempoEspera = Duration.ofSeconds(30);
			
			WebDriverWait wait = new WebDriverWait(driver, tempoEspera);
			Alert element = wait.until(ExpectedConditions.alertIsPresent());
		}

		/*
		 * Aguarda o elemento ser exibido (wait din�mico). Quando o mesmo estiver
		 * vis�vel, o teste ir� prosseguir. O tempo limite � de 30 segundos.
		 */
		public void aguardarPorElemento(By by) throws Exception {
			Duration tempoEspera = Duration.ofSeconds(30);
			
			WebDriverWait wait = new WebDriverWait(driver, tempoEspera);
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		}

		// Altera a janela corrente do teste pelo t�tulo da p�gina passado por
		// par�metro.
		public void alterarJanelaPorTituloPagina(String titulo) throws Exception {
			for (String winHandle : driver.getWindowHandles()) {
				if (driver.switchTo().window(winHandle).getTitle().equals(titulo)) {
					break;
				}
			}
			pausar(2000);
		}

		// Altera a janela corrente do teste pela URL da p�gina passada por
		// par�metro.
		public void alterarJanelaPorURL(String url) throws Exception {
			for (String winHandle : driver.getWindowHandles()) {
				if (driver.switchTo().window(winHandle).getCurrentUrl().equals(url)) {
					break;
				}
			}
			pausar(2000);
		}

		// Altera o foco para o frame 'ifrm'
		public void alterarFocoFrame() throws Exception {
			String frame = "ifrm";
			int frameZero = 0;

			try {
				driver.switchTo().frame(frame);
				
			} catch (NoSuchElementException e) {
				driver.switchTo().frame(frameZero);
			}
		}

		// Arrasta elemento e solta em um determinado local
		public void arrastarSoltarElemento(By elemento, By localFuturo) throws Exception {
			WebElement sourceLocator = driver.findElement(elemento);
			WebElement destinationLocator = driver.findElement(localFuturo);

			Actions actions = new Actions(driver);
			actions.dragAndDrop(sourceLocator, destinationLocator).build().perform();
	      }

		// Busca por um texto na p�gina atual
		public boolean buscarPorTextoNaPagina(String texto) throws Exception {
			try {
				boolean result = driver.getPageSource().contains(texto);
				return true;
			} catch (Exception e) {
				return false;
			}
		}

		/*
		 * Busca valores na combobox de acordo com a op��o desejada. N�o importa a
		 * posi��o em que o valor encontra-se, a busca ser� feita pelo valor desejado na
		 * combobox.
		 */
		public void buscarValoresNaComboBoxPorTexto(By cliqueSimplesNaCombo, By xpathDaListaElementos,
				String valorDesejadoNaCombo) throws Exception {
			
				WebElement findElement = driver.findElement((cliqueSimplesNaCombo));
				findElement.click();

				List<WebElement> elementos = driver.findElements((xpathDaListaElementos));

				for (WebElement elementosGrid : elementos) {
					if (elementosGrid.getText().equals(valorDesejadoNaCombo)) {

						elementosGrid.click();
				}
			}
		}

		// Clica no componente passado por par�metro.
		public void clicarElemento(By by) throws Exception {
			aguardarPorElemento(by);
			driver.findElement(by).click();
		}

		// Verifica se a propriedade �text� do componente em quest�o cont�m o texto
		// passado por par�metro.
		public void contemTextoPresentePorElemento(By by, String texto, String nomeElemento) throws Exception {
			Assert.assertTrue(driver.findElement(by).getText().contains(texto));
		} 

		// Deleta todos os cookies do navegador.
		public void deletarTodosCookies() {
			driver.manage().deleteAllCookies();
		}

		// N�o aceita o alerta (clica no bot�o Cancelar).
		public void dispensarAlerta() throws Exception {
			Alert alert = driver.switchTo().alert();
			alert.dismiss();
		} 

		// Realiza duplo clique em um elemento
		public void duploCliqueElemento(By by, String nomeElemento) throws Exception {
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(by);

			action.doubleClick(element).perform();
		}

		// Verifica se o componente est� vis�vel. Propriedade �isDisplayed�.
		public void elementoEstaVisivel(By by) throws NoSuchElementException {
			Assert.assertTrue(driver.findElement(by).isDisplayed());
		}

		// Verifica se o componente n�o est� vis�vel. Propriedade �isDisplayed�.
		public void elementoNaoEstaVisivel(By by) throws Exception {
			Assert.assertFalse(driver.findElement(by).isDisplayed());
		}

		// Verifica se o elemento est� presente.
		public boolean elementoEstaPresente(By by) throws NoSuchElementException {
			try {
				driver.findElement(by);
				return true;
			} catch (NoSuchElementException e) {
				return false;
			}
		}

		// Verifica se o elemento n�o est� presente.
		public void elementoNaoEstaPresente(By by) throws Exception {
			boolean notPresent = ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(by)).apply(driver);
			Assert.assertTrue(notPresent);
		}

		// Preenche campo de texto (componente) com texto passado por par�metro.
		public void escreverTexto(By by, String texto) throws Exception {
				aguardarPorElemento(by);
				driver.findElement(by).clear();
				driver.findElement(by).sendKeys(texto);
		}

		// Preenche campo de texto (componente) com texto passado por par�metro
		// utilizando comando javascript.
		public void escreverTextoComandoJavaScript(By by, String script, String texto, String nomeElemento)
			throws Exception {
			
			aguardarPorElemento(by);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(script); // exemplo de script:
			// document.getElementById('form').style=''
			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(texto);
		}

		// Verifica se existe alerta sendo exibido no momento.
		public void existeAlerta() throws Exception {
				driver.switchTo().alert();
			}

		// Realiza o file upload de um arquivo buscando da pasta 'upload'
		public void fileUpload(By by, String arquivo, String nomeElemento) throws Exception {
			String pathUpload = (userDir + "\\upload\\" + arquivo);
			
			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(pathUpload);
			}

		// Retorna um n�mero rand�mico entre 0 e 100.
		public int gerarNumeroRandomico() {
			Random random = new Random();
			int x = random.nextInt(101);

			return x;
		}

		// Limpa o campo de texto.
		public void limparCampoTexto(By by, String nomeElemento) throws Exception {
			aguardarPorElemento(by);
			driver.findElement(by).clear();
		}

		// Realiza a a��o 'Mouse Over' no elemento
		public void mouseOverNoElemento(By by) throws Exception {
			Actions actions = new Actions(driver);
			WebElement mouseHover = driver.findElement(by);
			actions.moveToElement(mouseHover).perform();
		}

		// Realiza a a��o 'Mouse Scroll' de baixo para cima na p�gina
		public void mouseScrollBaixoParaCima(By by) throws Exception {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,500)", "");
		}

		// Realiza a a��o 'Mouse Scroll' at� que o elemento seja visualizado
		public void mouseScrollBuscaElemento(By by) throws Exception {
			WebElement element = driver.findElement(by);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		}

		// Realiza a a��o 'Mouse Scroll' de cima para baixo na p�gina
		public void mouseScrollCimaParaBaixo(By by) throws Exception {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		}

		// Pausa a execu��o do teste de acordo com o tempo (milissegundos) passado
		// por par�metro.
		public void pausar(int tempo) throws Exception {
			Thread.sleep(tempo);
		}

		// Pressiona tecla de teclado passando por par�metro.
		public void pressionarTecla(By by, Keys keyboard) throws Exception {
			aguardarPorElemento(by);
			driver.findElement(by).sendKeys(keyboard);
		}

		// Realiza refresh na p�gina.
		public void refreshPagina() {
			driver.navigate().refresh();
		}

		/*
		 * Tira uma screenshot e a armazena na pasta ExtentReports com o nome passado
		 * por par�metro concatenando com �yyyyMMddHH�, no formato jpg.
		 */
		public static String screenShot(String fileName) {

			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

			String imagem =  fileName + "_" + Utils.gerarDataHoraAtual() + "_" + 
	                Utils.obterNomeBrowser() + "_" + 
	                Utils.obterVersaoBrowser() + "_" + 
	                Utils.osName + ".jpg";

			try {

				FileUtils.copyFile(scrFile, new File("screenshot/" + imagem), true);
				return imagem;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return imagem;
		}

		// Seleciona opção em combobox pelo texto passado por par�metro.
		public void selecionarElementoPorTexto(By by, String texto) throws Exception {
			aguardarPorElemento(by);
			new Select(driver.findElement(by)).selectByVisibleText(texto);
		}

		// Seleciona op��o em combobox pela propriedade value passada por par�metro.
		public void selecionarElementoPorValue(By by, String valor, String nomeElemento) throws Exception {
			aguardarPorElemento(by);
			new Select(driver.findElement(by)).selectByValue(valor);
		}

		// Seleciona op��o em combobox pela propriedade index passada por par�metro.
		public void selecionarElementoPorIndex(By by, int index, String nomeElemento) throws Exception {
			aguardarPorElemento(by);
			new Select(driver.findElement(by)).selectByIndex(index);
		}

		// Seleciona op��o em combobox pelo texto passado por par�metro utilizando
		// comando javascript.
		public void selecionarElementoPorTextoComandoJavaScript(By by, String script, String texto, String nomeElemento)
			throws Exception {
			
			pausar(2000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(script); // exemplo de script:
											// document.getElementById('form').style=''
			new Select(driver.findElement(by)).selectByVisibleText(texto);
		}

		// Seleciona op��o em combobox pela propriedade value passada por par�metro
		// utilizando comando javascript.
		public void selecionarElementoPorValueComandoJavaScript(By by, String script, String valor, String nomeElemento)
			throws Exception {
			
			pausar(2000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(script); // exemplo de script:
											// document.getElementById('form').style=''
			new Select(driver.findElement(by)).selectByValue(valor);
		}

		// Seleciona op��o em combobox pela propriedade index passada por par�metro
		// utilizando comando javascript.
		public void selecionarElementoPorIndexComandoJavaScript(By by, String script, int index, String nomeElemento)
				throws Exception {
			
			pausar(2000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(script); // exemplo de script:
											// document.getElementById('form').style=''
			new Select(driver.findElement(by)).selectByIndex(index);
		}

		// Verifica se o texto passado por par�metro n�o esta presente na
		// propriedade �text� do componente em quest�o.
		public void textoNaoPresentePorElemento(By by, String textoEsperado) throws Exception {
			Assert.assertFalse(driver.findElement(by).getText().contains(textoEsperado));
		}

		//// Verifica se o texto passado por par�metro esta presente na propriedade
		//// �text� do componente em quest�o.
		public void textoPresentePorElemento(By by, String textoEsperado) throws Exception {
			Assert.assertEquals(textoEsperado, driver.findElement(by).getText());
		}

		// Verifica se o componente em quest�o est� selecionado (checkbox por
		// exemplo).
		public void validarComponenteSelecionado(By by, String nomeElemento) throws Exception {
			Assert.assertTrue(driver.findElement(by).isSelected());
		}

		// Verifica se o componente em quest�o est� habilitado.
		public void validarComponenteHabilitado(By by, String nomeElemento) throws Exception {
			Assert.assertTrue(driver.findElement(by).isEnabled());
		}

		// Verifica se o componente em quest�o n�o est� selecionado (checkbox por
		// exemplo).
		public void validarComponenteNaoSelecionado(By by, String nomeElemento) throws Exception {
			Assert.assertFalse(driver.findElement(by).isSelected());
		}

		// Verifica se o componente em quest�o n�o est� habilitado.
		public void validarComponenteDesabilitado(By by, String nomeElemento) throws Exception {
			Assert.assertFalse(driver.findElement(by).isEnabled());
		}

		/*
		 * Ler dois arquivos de texto, verifica as diferen�as do primeiro para o segundo
		 * e do segundo para o primeiro arquivo e valida se o conte�do dos arquivos s�o
		 * iguais.
		 */
		public void validarExisteDiferencaEntreDoisArquivosTxt(String primeiroArquivo, String segundoArquivo)
				throws Exception {

			List<String> diferencaPrimeiroParaSegundo = new ArrayList<String>();
			List<String> diferencaSegundoParaPrimeiro = new ArrayList<String>();

			List<String> linhasA = Files.readAllLines(Paths.get(primeiroArquivo));
			List<String> linhasB = Files.readAllLines(Paths.get(segundoArquivo));

			diferencaPrimeiroParaSegundo.addAll(linhasA);
			diferencaPrimeiroParaSegundo.removeAll(linhasB);

			diferencaSegundoParaPrimeiro.addAll(linhasB);
			diferencaSegundoParaPrimeiro.removeAll(linhasA);

			if (!linhasA.equals(linhasB)) {
				throw new Exception();
			} 
		}

		// Valida se na URL atual cont�m o texto (trecho da URL).
		public void validarURLAtualContemString(String trechoURL) throws Exception {
			Assert.assertTrue(driver.getCurrentUrl().contains(trechoURL));
		}

		// Retorna � tela anterior.
		public void voltarTelaAnterior() throws Exception {
			driver.navigate().back();
		}

		public static String gerarDataHoraAtual() {

			// Configura objeto do tipo Date com o padr�o yyyyMMdd
			String cData = "yyyyMMdd";
			DateFormat formataData = new SimpleDateFormat("yyyyMMddHHmm");
			Date dDate = new Date();
			cData = formataData.format(dDate);

			return cData;
		}

		public static String obterVersaoBrowser() {
			Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
			Object strBrowserVersion = capabilities.getBrowserVersion();
			if (strBrowserVersion == "") {
				strBrowserVersion = capabilities.getCapability("browserVersion");
			} else {
				return (String) strBrowserVersion;
			}
			return (String) strBrowserVersion;
		}

		public static String obterNomeBrowser() {
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();

			return browserName;
		}
}
